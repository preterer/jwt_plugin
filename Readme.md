#Authentication plugin

##Opis
Plugin umożliwia prostą obsługę tokenów w dowolnym formacie. Aktualnie obsługje
dekodowanie tokenów JWT. Można również używać swojego dekodera.

##Użytkownik

###Instalacja jako plugin
1. Do dependencies w `bower.json` dodajemy linię
`"jwt_plugin": "git@bitbucket.org:preterer/jwt_plugin.git"`

2. Dodajmy do listy plików (gruntfile / lista plików w indexie)

3. Dodajemy zależność do modułu angulara `angular.module("app_name", ["jwt_plugin"])`

###Użycie

- Rejestrujemy interceptor `JWTPluginInterceptor`
- [API](api/Api.md)

##Developer

###Instalacja dla developera

1. `npm install`
2. `bower install`

###Uruchamianie aplikacji dla developera
Aplikację uruchamiamy poleceniem `grunt`. Dodatkowe parametry:

- `test` - uruchamia testy
- `dist` - wykonuje testy, a następnie tworzy paczkę dist
- `build` - wykonuje samo budowanie
