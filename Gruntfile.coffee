BOWER_DIR = "./bower_components"
BUILD_DIR = "./build"
TARGET_DIR = "./target"
SRC_DIR = "./src"
DIST_DIR = "./dist"
TEST_DIR = "./tests"

module.exports = (grunt) ->
  PROFILE = grunt.option('profile') || 'devel'

  grunt.initConfig
    clean:
      all: ["#{BUILD_DIR}", "#{TARGET_DIR}"]

    copy:
      main:
        files: [
          {
            expand: true
            cwd: "#{SRC_DIR}/main"
            src: ["**"]
            dest: "#{BUILD_DIR}/src"
          }
        ]
      profile:
        files: [
          {
            expand: true
            cwd: "{SRC_DIR}/profile/" + PROFILE
            src: ["**"]
            dest: "#{BUILD_DIR}/src"
          }
        ]
      images:
        files: [
          {
            expand: true
            cwd: "#{SRC_DIR}/images"
            src: ["**"]
            dest: "#{TARGET_DIR}/app/images"
          }
        ]
      dist:
        files: [
          {
            src: "#{TARGET_DIR}/app/app.min.js"
            dest: "#{DIST_DIR}/jwt_plugin.min.js"
          }
        ]
      docs:
        files: [

        ]

    concat:
      libjs:
        options:
          banner: "/*!TADAM*/\n"
        files:[
          {
            src: [
              "#{BOWER_DIR}/angular/angular.min.js"
              "#{BOWER_DIR}/angular-cookie/angular-cookie.min.js"
              "#{BOWER_DIR}/angular-jwt/dist/angular-jwt.min.js"
              "#{BOWER_DIR}/angular-mocks/angular-mocks.js"
            ]
            dest: "#{TARGET_DIR}/app/lib.js"
          }
        ]

      libcss:
        options:
          banner: "/*!TADAM*/\n"
        files: [
          {
            src: [
              "#{BOWER_DIR}/angular/angular-csp.css"
              "#{BOWER_DIR}/bootstrap/dist/css/bootstrap.min.css"
            ]
            dest: "#{TARGET_DIR}/app/lib.css"
          }
        ]

      coffee:
        options:
          banner: "###TADAM###\n"
        files: [
          {
            src: [
              "#{BUILD_DIR}/src/coffee/main.coffee"
              "#{BUILD_DIR}/src/coffee/**/*.coffee"
            ]
            dest: "#{BUILD_DIR}/src/app.coffee"
          }
        ]

    coffee:
      app:
        src: "#{BUILD_DIR}/src/app.coffee"
        dest: "#{BUILD_DIR}/src/app.js"
      unit:
        options:
          bare: true
        expand: true
        flatten: true
        cwd: "#{TEST_DIR}/unit"
        src: ['**/*.coffee']
        dest: "#{TARGET_DIR}/test/unit"
        ext: '.js'
      allWithLitCoffee:
        files: [
          {
            dest: "#{BUILD_DIR}/src/app.js"
            src: ["#{SRC_DIR}/main/coffee/main.coffee", "#{SRC_DIR}/main/coffee/**/*.coffee", "#{SRC_DIR}/main/coffee/**/*.coffee.md"]
          }
        ]

    sass:
      app:
        src: "#{BUILD_DIR}/src/sass/imports.sass"
        dest: "#{TARGET_DIR}/app/app.css"

    jade:
      app:
        options:
          pretty: true
        src: "#{BUILD_DIR}/src/jade/index.jade"
        dest: "#{TARGET_DIR}/app/index.html"

    uglify:
      all:
        files: [
          {
            src: "#{BUILD_DIR}/src/app.js"
            dest: "#{TARGET_DIR}/app/app.min.js"
          }
        ]

    html2js:
      options:
        base: "#{BUILD_DIR}/src/jade/"
        jade:
          doctype: "html"
          watch: true
      app:
        files: [
          src: ["#{BUILD_DIR}/src/jade/**/*.jade", "!#{BUILD_DIR}/src/jade/index.jade"]
          dest: "#{TARGET_DIR}/app/templates.js"
        ]

    connect:
      server:
        options:
          port: 8000
          hostname: "*"
          base: "#{TARGET_DIR}/app/"

    watch:
      all:
        files: ["#{SRC_DIR}/**"]
        tasks: ["build", "doTests"]

    jasmine:
      unit:
        src: ["#{TARGET_DIR}/app/lib.js", "#{TARGET_DIR}/app/app.min.js"]
        options:
          specs: "#{TARGET_DIR}/test/unit/**"

  grunt.loadNpmTasks('grunt-contrib-clean')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-contrib-sass')
  grunt.loadNpmTasks('grunt-contrib-jade')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-html2js')
  grunt.loadNpmTasks('grunt-contrib-connect')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-contrib-jasmine')

  grunt.registerTask 'concatAll', ["concat:libjs"]
  grunt.registerTask 'copyAll', ["copy:main", "copy:profile", "copy:images"]
  grunt.registerTask 'coffeeAll', ["coffee:allWithLitCoffee", "coffee:unit"]

  grunt.registerTask 'build', ["clean:all", "copyAll", "concatAll", "coffeeAll", "uglify:all"]

  grunt.registerTask 'unitTest', ["jasmine:unit"]
  grunt.registerTask 'e2eTest', []
  grunt.registerTask 'doTests', ["unitTest", "e2eTest"]
  grunt.registerTask 'test', ["build", "doTests"]

  grunt.registerTask 'run', ["build", "connect:server", "watch:all"]
  grunt.registerTask 'dist', ["test", "copy:dist"]

  grunt.registerTask 'default', ["run"]
