describe """TokenCache""", ->
    beforeEach module('jwt_plugin')

    TokenCache = null
    beforeEach inject (_TokenCache_) ->
      TokenCache = _TokenCache_

    cookieName = "jwt-token"
    nonJwtToken = "token"
    jwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
    "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9." +
    "TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"

    beforeEach ->
      TokenCache.removeToken()

    describe """setToken(token)""", ->
      it """should save token into cookies""", ->
        spyOn(TokenCache, 'ipCookie')
        TokenCache.setToken(nonJwtToken)
        expect(TokenCache.ipCookie).toHaveBeenCalledWith(cookieName, nonJwtToken)

    describe """getToken()""", ->
      it """should return previously set token""", ->
        TokenCache.setToken(nonJwtToken)
        expect(TokenCache.getToken()).toBe nonJwtToken

    describe """removeToken()""", ->
      it """should remove previously set token""", ->
        TokenCache.setToken(nonJwtToken)
        TokenCache.removeToken()
        expect(TokenCache.getToken()).toBeUndefined()
