describe """Decoder""", ->
    beforeEach module('jwt_plugin')

    Decoder = null
    beforeEach inject (_Decoder_) ->
      Decoder = _Decoder_


    nonJwtToken = "token"
    jwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
    "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9." +
    "TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"

    describe """decode(token)""", ->
      it """should decode jwt token""", ->
        expect(Decoder.decode(jwtToken)).toEqual {
          sub: '1234567890'
          name: 'John Doe'
          admin: true
        }

      it """should return empty object for a non jwt token""", ->
        expect(Decoder.decode(nonJwtToken)).toEqual {}
