describe """Auth""", ->
    beforeEach module('jwt_plugin')

    Auth = null
    beforeEach inject (_Auth_) ->
      Auth = _Auth_

    beforeEach ->
      Auth.unauthorize()

    nonJwtToken = "token"
    jwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
    "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9." +
    "TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
    jwtTokenKey = "sub"

    describe """setToken(token)""", ->
      it """should save token and decoded data""", ->
        spyOn(Auth.tokenCache, 'setToken')
        spyOn(Auth.decoder, 'decode')
        Auth.setToken(nonJwtToken)
        expect(Auth.tokenCache.setToken).toHaveBeenCalledWith(nonJwtToken)
        expect(Auth.decoder.decode).toHaveBeenCalledWith(nonJwtToken)
        expect(Auth.token).toBe(nonJwtToken)
        expect(Auth.data).not.toBeNull()


    describe """getToken()""", ->
      it """should return null when there is no token set""", ->
        expect(Auth.getToken()).toBeNull()

      it """should return a previously set token""", ->
        Auth.setToken(nonJwtToken)
        expect(Auth.getToken()).toBe(nonJwtToken)


    describe """isAuthorized()""", ->
      it """should return true when there is a token""", ->
        Auth.setToken(nonJwtToken)
        expect(Auth.isAuthorized()).toBeTruthy()

      it """should return false when there is no token""", ->
        expect(Auth.isAuthorized()).toBeFalsy()


    describe """getParam(key)""", ->
      it """should return the JWT token value of the key if it's present""", ->
        Auth.setToken(jwtToken)
        #console.log(Auth.getParam(jwtTokenKey))
        expect(Auth.getParam(jwtTokenKey)).not.toBeUndefined()

      it """should return undefined when there is no token""", ->
        expect(Auth.getParam(jwtTokenKey)).toBeUndefined()

      it """should return undefined when token is not JWT""", ->
        Auth.setToken(nonJwtToken)
        expect(Auth.getParam(jwtTokenKey)).toBeUndefined()

    describe """unauthorize()""", ->
      it """should clear saved data with non JWT token""", ->
        spyOn(Auth.tokenCache, 'removeToken')
        Auth.setToken(nonJwtToken)
        Auth.unauthorize()
        expect(Auth.token).toBeNull()
        expect(Auth.data).toBeNull()
        expect(Auth.tokenCache.removeToken).toHaveBeenCalled()
