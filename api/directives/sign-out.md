#sign-out

##Description
Unauthorizes user, on click.

##Usage
Add `sign-out` to your html element

##Example
  - HTML: `<p sign-out>Log out</p>`
  - JADE: `p(sign-out) Log out`

#[Back](../Api.md)
