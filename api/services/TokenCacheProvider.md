#TokenCacheProvider ([TokenCache](TokenCache.md))

##Description
Provider for TokenCache

##Methods
###setCookieName(cookieName)
####Description
Sets name of the cookie holding cached token

####Params

- `cookieName` - name of the cookie to set

#[Back](../Api.md)
