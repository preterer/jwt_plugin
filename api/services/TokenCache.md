#TokenCache ([provider](TokenCacheProvider.md))
##Description
Service used for token caching and restoring.

##Methods

###setToken(token)
####Description
Saves token

####Params

- `token` - token to save

---

###getToken()
####Description
Gets previously saved token

---

###removeToken()
####Description
Removes token from cache

#[Back](../Api.md)
