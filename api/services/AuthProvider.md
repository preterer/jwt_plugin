#AuthProvider ([Auth](Auth.md))

##Description
Provider for Auth service

##Methods

###setHeaderName(headerName)
####Description
Used to set a custom authentication header name

####Params

- `headerName` - new authentication header name

---

###setCustomDecoder(decoder)
####Description
Used to set a custom decoder for token

####Params

1. `decoder` - a decoder object, which has to implement:
    * `decode`: `function(token) : Object`

---

###setCustomCache(cache)
####Description
Used to set a custom cache system

####Params

1. `cache` - a cache object, which has to implement:
    * `setToken`: `function(token) : void`
    * `getToken`: `function() : Object`
    * `removeToken`: `function() : void`

#[Back](../Api.md)
