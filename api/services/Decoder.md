#Decoder

##Description
Decoder service. Used as default token decoder in Auth. All custom decoders
has to implement it's interface.

##Methods

###decode(token)
####Description
Decodes token using jwtHelper service

####Params

- `token` - a token to decode. If specified token is not JWT token, the result
will be `{}`

#[Back](../Api.md)
