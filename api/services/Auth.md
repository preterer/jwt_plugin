#Auth ([provider](AuthProvider.md))

##Description
Authorization service. Uses tokens to check if user is authorized, uses [TokenCache]()
to keep token between sessions.

##Methods

###setToken(token)
####Description
Used to set token on demand. Is used in main Interceptor.

####Params
- `token` - token to save and decode

---
###getToken()
####Description
Used to get currently set token

---
###isAuthorized()
####Description
Used to check if token is present

---
###getParam(key)
####Description
Used to get a param from the decoded token object

####Params
- `key` - key to find in the data object

---
###unauthorize()
####Description
Removes token from servie, and from Cache

#[Back](../Api.md)
