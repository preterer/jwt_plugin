#Directives

##[sign-out](directives/sign-out.md)

#Services

##[Auth](services/Auth.md) ([provider](services/AuthProvider.md))
##[Decoder](services/Decoder.md)
##[TokenCache](services/TokenCache.md) ([provider](services/TokenCacheProvider.md))
