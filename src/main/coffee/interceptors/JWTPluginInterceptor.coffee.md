#JWTPluginInterceptor

##Description
Basic authorization interceptor.

    angular.module("jwt_plugin").factory "JWTPluginInterceptor", [
      "$injector"
      "$q"
      "Auth"
      ($injector, $q, Auth) -> {

##Events

###request

####Description
Sets header if user is authorized

        request: (config) ->
          if Auth.isAuthorized()
            config.headers[Auth.headerName] = Auth.getToken()
          return config or $q.when(config)

###response

####Description
Gets token from header if present

        response: (config) ->
          token = config.headers(Auth.headerName.toLowerCase())
          if token
            Auth.setToken(token)
          return config or $q.when(config)

###responseError

####Description
Unauthorizes if response status is Unauthorized

        responseError: (config) ->
          if config.status is 401
            Auth.unauthorize()
          return $q.reject(config)
      }
    ]
