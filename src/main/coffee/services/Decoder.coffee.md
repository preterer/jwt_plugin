#Decoder

##Description
Decoder service. Used as default token decoder in Auth. All custom decoders
has to implement it's interface.

    class Decoder
      constructor: (@decoder) -> return

##Methods

###decode(token)
####Description
Decodes token using jwtHelper service

      decode: (token) ->
        try
          return @decoder.decodeToken(token)
        catch error
          return {}

####Params

- `token` - a token to decode. If specified token is not JWT token, the result
will be `{}`

####Returns `object (map <string, object>)`

---

##Initialization

    angular.module("jwt_plugin").service "Decoder", [
      "jwtHelper"
      Decoder
    ]
