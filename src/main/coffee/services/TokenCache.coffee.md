#TokenCache ([provider](TokenCacheProvider.md))
##Description
Service used for token caching and restoring.

    class TokenCache
      constructor: (@cookieName, @ipCookie) -> return

##Methods

###setToken(token)

####Description
Saves token

      setToken: (token) ->
        @ipCookie(@cookieName, token)
        return

####Params

- `token` - token to save

####Returns `nothing`

---


###getToken()

####Description
Gets previously saved token

      getToken: ->
        return @ipCookie(@cookieName)

####Returns `string`

---

###removeToken()

####Description
Removes token from cache

      removeToken: ->
        @ipCookie.remove(@cookieName)
        return

####Returns `nothing`

#TokenCacheProvider ([TokenCache](TokenCache.md))

##Description
Provider for TokenCache

    class TokenCacheProvider
      cookieName = "jwt-token"

##Methods

###setCookieName(cookieName)

####Description
Sets name of the cookie holding cached token

      setCookieName: (name) ->
        cookieName = name
        return

####Params

- `cookieName` - name of the cookie to set

####Returns `nothing`

---

##Declaration

      $get: [
        "ipCookie"
        (ipCookie) ->
          return new TokenCache(cookieName, ipCookie)
      ]

    angular.module("jwt_plugin").provider "TokenCache", TokenCacheProvider
