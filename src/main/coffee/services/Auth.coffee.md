#Auth ([provider](AuthProvider.md))

##Description
Authorization service. Uses tokens to check if user is authorized, uses [TokenCache]()
to keep token between sessions.


    class Auth
      constructor: (@decoder, @headerName, @tokenCache) ->
        @token = @tokenCache.getToken()
        @data = @decoder.decode(@token)

##Methods

###setToken(token)

####Description
Used to set token on demand. Is used in main Interceptor.

      setToken: (token) ->
        @token = token
        @data = @decoder.decode(token)
        @tokenCache.setToken(token)
        return

####Params
- `token` - token to save and decode

####Returns `nothing`

---

###getToken()

####Description
Used to get currently set token

      getToken: ->
        return @token

####Returns `string`

---

###isAuthorized()

####Description
Used to check if token is present

      isAuthorized: ->
        return @token? and @token.length

####Returns `boolean`

---

###getParam(key)

####Description
Used to get a param from the decoded token object

      getParam: (key) ->
        if @token and @data
          return @data[key]

####Params

- `key` - key to find in the data object

####Returns `any`

---

###unauthorize()

####Description
Removes token from servie, and from Cache

      unauthorize: ->
        @token = null
        @data = null
        @tokenCache.removeToken()
        return

####Returns `nothing`


#AuthProvider ([Auth](Auth.md))

##Description
Provider for Auth service

    class AuthProvider
      headerName = "Authentication"
      customDecoder = null
      customCache = null

##Methods

###setHeaderName(headerName)

####Description
Used to set a custom authentication header name

      setHeaderName: (name) ->
        headerName = name
        return

####Params

- `headerName` - new authentication header name

####Returns `nothing`

---

###setCustomDecoder(decoder)

####Description
Used to set a custom decoder for token

      setCustomDecoder: (decoder) ->
        customDecoder = decoder

####Returns `nothing`

####Params

1. `decoder` - a decoder object, which has to implement:
    * `decode`: `function(token) : Object`

---

###setCustomCache(cache)
####Description
Used to set a custom cache system

      setCustomCache: (cache) ->
        customCache = cache

####Params

1. `cache` - a cache object, which has to implement:
    * `setToken`: `function(token) : void`
    * `getToken`: `function() : Object`
    * `removeToken`: `function() : void`

####Returns `nothing`

---

##Initialization

      $get: [
        "Decoder"
        "TokenCache"
        (Decoder, TokenCache) ->
          finalDecoder = customDecoder || Decoder
          finalCache = customCache || TokenCache
          return new Auth(finalDecoder, headerName, finalCache)
      ]
    angular.module("jwt_plugin").provider "Auth", AuthProvider
