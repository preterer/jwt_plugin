#sign-out

##Description
Unauthorizes user, on click.

##Usage
Add `sign-out` to your html element

##Example
  - HTML: `<p sign-out>Log out</p>`
  - JADE: `p(sign-out) Log out`

---

    angular.module("jwt_plugin").directive "signOut", [
      "Auth"
      (Auth) -> {
        restrict: 'A'
        link: ($scope, element) ->
          element.on "click", (event) ->
            Auth.unauthorize()
      }
    ]
